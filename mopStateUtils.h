#pragma once
/*
* This struct is used to store a single state that contains many MopItems, for when reading from a MOP file.
* creates co-existed scaled array if not being used for file writing.
*/

#include "mopState.h"


void addToMopState (mopState *in, mopItem item) {
    if (in == NULL) {
      std::cout << "> addToMopState called with non allocated mopState - exiting " << std::endl;
      exit(0);
    }
  int size = in->size;
  if (size==0) {
    // we need to create the first array element and initialise it from the one that was passed
    in->content =  new mopItem[1];

    in->content[0] = item;
    in->size = 1;
  } else {
    // If we got here, then the array has been created, so we can safely extend it
   in->size++;
   mopItem * content =  new mopItem[in->size];
   for (int x(0);x<in->size-1;x++) {
     content[x] = in->content[x];
   }
   content[in->size-1] = item;
   delete [] in->content;
   in->content = content;
   content = NULL;
  }
}
